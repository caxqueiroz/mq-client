package mq_client

import java.util.Properties
import javax.naming.{InitialContext, Context}
import javax.jms._
import java.sql.{DriverManager, Connection}
import java.util.logging.Logger


/**
 * User: cq
 * Date: 7/10/13
 * Time: 4:24 PM
 */
object JMSConsumerApp {

	val NON_TRANSACTED = false

	def saveTime(arrival: Long, message: String, con: Connection): Int = {

		val stmt = con.prepareStatement("insert into measurements (arrival, message) values (?,?)")
		stmt.setLong(1,arrival)
		stmt.setString(2,message)

		val r = stmt.executeUpdate()
		stmt.close()
		r
	}



	def conn = {
		Class.forName("org.sqlite.JDBC")
		DriverManager.getConnection("jdbc:sqlite:/tmp/subscriber-perf.db")
	}

	def createTable(dbConn: Connection) = {
		val stmt = dbConn.prepareStatement("drop table if exists measurements")
		stmt.executeUpdate()
		stmt.close()
		val stmt2 = dbConn.prepareStatement("CREATE TABLE \"measurements\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, \"client\" TEXT NOT NULL, \"timeToSend\" DOUBLE NOT NULL)")
		stmt2.executeUpdate()
		stmt2.close()
	}

	def main(args: Array[String]) {

		try {

			val LOG = Logger.getLogger(getClass.getName)

      if(args.length != 1){
        println("usage: JMSConsummerApp <broker url> (e.g. tcp://xx.xx.xx.xx:yyyy)")
        System.exit(1)
      }

			val props = new Properties()
			props.setProperty(Context.INITIAL_CONTEXT_FACTORY,"org.apache.activemq.jndi.ActiveMQInitialContextFactory")
			props.setProperty(Context.PROVIDER_URL,args(0))
			val ctx = new InitialContext(props)

			// lookup the connection factory
			val connectionFactory =  ctx.lookup("ConnectionFactory").asInstanceOf[ConnectionFactory]

			val connection = connectionFactory.createConnection()

			connection.start()
			val dbConn = conn
			createTable(dbConn)

			val session = connection.createSession(NON_TRANSACTED, Session.AUTO_ACKNOWLEDGE)

			val destination = session.createTopic("mqtt-topic")

			val consumer = session.createConsumer(destination)



			consumer.setMessageListener(new MessageListener {
				def onMessage(p1: Message){
					val arrival = System.currentTimeMillis()
					val textMessage = p1.asInstanceOf[TextMessage]
          val text = textMessage.getText
					saveTime(arrival,text,dbConn)
					LOG.finest("Arrival at " + arrival + " " + text)

				}
			})

			val lock = new Object()
			lock.synchronized {
				lock.wait()
			}

			consumer.close()
			session.close()
			connection.close()

		} catch  {
			case e : Exception => {
				println("Caught exception!")
				println(e.getMessage)
			}


		}

	}

}
