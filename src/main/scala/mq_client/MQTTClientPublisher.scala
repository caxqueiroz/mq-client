package mq_client

import org.fusesource.mqtt.client.{Callback, QoS, Topic, MQTT}
import java.util.concurrent.{TimeUnit, ExecutorService, Executors}
import java.util.logging.Logger
import java.sql.{Connection, DriverManager}
import scala.util.parsing.json.{JSONObject, JSON}

/**
 * User: cq
 * Date: 2/10/13
 * Time: 4:13 PM
 */
object MQTTClientPublisher {

	val NUM_MESSAGES_TO_SEND = 10000

	def main(args: Array[String]) {
		if(args.length != 3){
			println("usage: MQTTClientPublisher <num threads> <host> <port>")
			System.exit(1)
		}
		val startTime = System.currentTimeMillis()
		val numThreads = Integer.valueOf(args(0))
		val pool: ExecutorService = Executors.newFixedThreadPool(numThreads)
		val dbConn = conn
		createTable(dbConn)

		for(i <- 1 to numThreads){
			pool.execute(new Handler(args(1),Integer.valueOf(args(2)),NUM_MESSAGES_TO_SEND,dbConn))
		}
		pool.shutdown()
		pool.awaitTermination(Long.MaxValue, TimeUnit.NANOSECONDS)

		val stopTime = System.currentTimeMillis()
		val procTime = ((stopTime - startTime)/1000)
		saveTime("Total time (secs)",procTime,dbConn)
		dbConn.close()

		println("Total time (secs): " + procTime)

	}

	class Handler(host: String, port: Int, num_messages: Int, dbConn:Connection) extends Runnable {
		val LOG = Logger.getLogger(getClass.getName)
		def threadName = Thread.currentThread.getName()

		val jsonMessage = "{\n    \"observation\": {\n        \"id\": \"0000c0df85142214_01\",\n        \"phenomenonTime\": {\n            \"start\": \"2011-01-27T06:30:16.000+11:00\",\n            \"finish\": \"2011-01-27T06:30:16.000+11:00\"\n        },\n        \"procedure\": { \"href\": \"http://www.Sense-T.org.au/id/source/csiro/stream/my_stream_324234234\" },\n        \"type\": { \"href\": \"http://www.Sense-T.org.au/id/message-format/core-timeseries\" },\n        \"result\": {\n            \"value\": 3.0,\n            \"metadata\": [\n                { \"key\":\"type\",\"value\":\"soil temperature\" },\n                { \"key\":\"unit\",\"value\": \"K\" },\n                { \"key\":\"depth\",\"value\": 1.5 },\n                { \"key\":\"latitude\",\"value\": 172.37 },\n                { \"key\":\"longitude\",\"value\": -37.32 },\n                { \"key\":\"owner\",\"value\": \"hydro tasmania\" },\n                { \"key\":\"routingpath\", \"value\": [\n                        { \"node\": “0000c0df85142229\", \"rssi\": -84 }, //this is the gateway\n                        { \"node\": \"0000c0df8514223a\", \"rssi\": -99 },\n                        { \"node\": \"0000c0df851422b4\", \"rssi\": -57 }\n                    ]\n                }\n            ]\n        }\n    }\n}"


		def run() {

			LOG.info(threadName + " is running.")
			val mqttService = new MQTT()
			mqttService.setHost(host, port)
			val connection = mqttService.blockingConnection()
			connection.connect()



			for(i <- 1  to  num_messages){
				val start = System.currentTimeMillis()
				val message = new JSONObject(Map("payload" -> jsonMessage, "time" -> System.currentTimeMillis()))

				connection.publish("topic-0",message.toString().getBytes,QoS.AT_LEAST_ONCE,true)
				val end = System.currentTimeMillis()
				saveTime(threadName,(end - start),dbConn)
				//LOG.info("Message sent (ms) " + (end - start))
//				Thread.sleep(100)
			}
			connection.disconnect()
			LOG.info(threadName + "finished running.")
		}
	}

	def saveTime(threadName: String, time: Double, con: Connection): Int = {

		val stmt = con.prepareStatement("insert into measurements (client, timeToSend) values (?,?)")
		stmt.setString(1,threadName)
		stmt.setDouble(2,time)
		val r = stmt.executeUpdate()
		stmt.close()
		r
	}

	def createTable(dbConn: Connection) = {
		val stmt = dbConn.prepareStatement("drop table if exists measurements")
		stmt.executeUpdate()
		stmt.close()
		val stmt2 = dbConn.prepareStatement("CREATE TABLE \"measurements\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, \"client\" TEXT NOT NULL, \"timeToSend\" DOUBLE NOT NULL)")
		stmt2.executeUpdate()
		stmt2.close()
	}

	def conn = {
		Class.forName("org.sqlite.JDBC")
		DriverManager.getConnection("jdbc:sqlite:/tmp/perf.db")
	}

}
