package mq_client

import org.fusesource.mqtt.client._
import org.fusesource.hawtbuf._
import java.sql.{DriverManager, Connection}
import scala.util.parsing.json.{JSONArray, JSON, JSONObject}
import scala.collection.immutable.TreeMap

/**
 * App that consumes mqtt messages.
 * User: cq
 * Date: 30/07/13
 * Time: 9:15 AM
 */
object MQTTClientSubscriber {

	def saveTime(arrival: Long, message: String, con: Connection): Int = {

		val stmt = con.prepareStatement("insert into measurements (arrival, message) values (?,?)")
		stmt.setLong(1,arrival)
		stmt.setString(2,message)

		val r = stmt.executeUpdate()
		stmt.close()
		r
	}

	def conn = {
		Class.forName("org.sqlite.JDBC")
		DriverManager.getConnection("jdbc:sqlite:subscriber-perf.db")
	}


  def main(args: Array[String]) {
    if(args.length != 2){
      println("usage: MQTTClientSubscriber <host> <port>")
      System.exit(1)
    }
    val mqtt = new MQTT()
    mqtt.setHost(args(0), Integer.parseInt(args(1)))
    val connection = mqtt.callbackConnection()
    val topic = new Topic("029282",QoS.AT_LEAST_ONCE)
    val topics = Array[Topic]{topic}
//    val dbConn = conn

    connection.listener(new Listener {
      def onPublish(p1: UTF8Buffer, p2: Buffer, p3: Runnable) {
        p3.run()
    	val jsonMessage = new String(p2.buffer().toByteArray)
        val arrival = System.currentTimeMillis()
        //saveTime(arrival,jsonMessage,dbConn)
		println("Arrival at " + arrival + " " + jsonMessage)

      }

      def onConnected() {
        println("connected")
      }

      def onFailure(p1: Throwable) {

      }

      def onDisconnected() {
        println("disconnected")
//        dbConn.close()
      }
    })

    connection.connect(new Callback[Void] {
      def onFailure(p1: Throwable) {}

      def onSuccess(p1: Void) {
        println("connected")

//        connection.publish("sense-t","test 123".getBytes,QoS.AT_LEAST_ONCE,false,null)

        connection.subscribe(topics,new Callback[Array[Byte]] {
          def onFailure(p1: Throwable) {
            println("subscription failed!!")
            println(p1.getMessage)
          }

          def onSuccess(p1: Array[Byte]) {
            println("subscribed successfully!!")
          }
        })
      }
    })

    val lock = new Object()
    lock.synchronized {
      lock.wait()
    }

  }
}
