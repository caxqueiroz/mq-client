package comp90056

import org.fusesource.mqtt.client.{QoS, MQTT}

/**
 * User: cq
 * Date: 16/10/13
 * Time: 8:24 PM
 */
object RatingsAdmin {

  def main(args: Array[String]) {
    if(args.length != 3){
      println("usage: RatingsAdmin <broker> <port> <topic> (e.g. 119.81.5.2 1884 029282)")
      System.exit(1)
    }

    val hostname = args(0)
    val port = Integer.parseInt(args(1))

    val mqttService = new MQTT()
    mqttService.setHost(hostname, port)

    val connection = mqttService.blockingConnection()
    connection.connect()
    val message = "START," + args(2)
    connection.publish("comp90056",message.toString().getBytes,QoS.AT_LEAST_ONCE,true)
    connection.disconnect()

  }

}
