package comp90056

import java.sql.{Connection, DriverManager}
import java.util.concurrent.{ConcurrentLinkedQueue, Executors, ExecutorService}
import java.util.logging.Logger
import org.fusesource.mqtt.client.{Callback, Listener, QoS, MQTT}
import org.fusesource.mqtt.client
import org.fusesource.hawtbuf.{Buffer, UTF8Buffer}
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
 * User: cq
 * Date: 16/10/13
 * Time: 12:44 PM
 */
object RatingsProducer {


	def conn(database:String) = {
		Class.forName("org.sqlite.JDBC")
		DriverManager.getConnection("jdbc:sqlite:" + database)
	}

	def loadData(sql: String, dbConn: Connection): ListBuffer[Payload] = {

		val stm = dbConn.prepareStatement(sql)
		val rs = stm.executeQuery()
		val ratings = ListBuffer[Payload]()
		while(rs.next()){

			val userid = rs.getInt("user_id")
			val venueid = rs.getInt("venue_id")
			val rating = rs.getInt("rating")
			val payload = new Payload(userid,venueid,rating)
			ratings += payload
		}
		ratings
	}


	def main(args: Array[String]) {

		//mvn exec:java -Dexec.mainClass="comp90056.RatingsProducer" -Dexec.args="119.81.5.2 1884 /Users/cq/Downloads/umn_foursquare_datasets/fsdata.db"
		if(args.length != 3){
			println("usage: mvn exec:java -Dexec.mainClass=\"comp90056.RatingsProducer\" -Dexec.args=\"<host> <port> <db>\"")
			System.exit(1)
		}

		val LOG = Logger.getLogger(getClass.getName)

		val dbConn = conn(args(2))
		val pool: ExecutorService = Executors.newFixedThreadPool(27)

		val runnings = new mutable.HashMap[String,Boolean]()

		LOG.info("Loading data...")

		val payloads = loadData("select user_id, venue_id, rating from ratings",dbConn)

		LOG.info("Data loaded!!!")

		val mqtt = new MQTT()
		mqtt.setHost(args(0), Integer.parseInt(args(1)))
		val connection = mqtt.callbackConnection()
		val topic = new client.Topic("comp90056",QoS.AT_LEAST_ONCE)
		val topics = Array[client.Topic]{topic}



		connection.connect(new Callback[Void] {
			def onFailure(p1: Throwable) {
				println("failure " + p1)
			}

			def onSuccess(p1: Void) {
				println("connected")

				connection.subscribe(topics,new Callback[Array[Byte]] {
					def onFailure(p1: Throwable) {
						println("subscription failed!!")
						println(p1.getMessage)
					}

					def onSuccess(p1: Array[Byte]) {
						println("subscribed successfully!!")
					}
				})
			}
		})

		connection.listener(new Listener {

			def onPublish(p1: UTF8Buffer, p2: Buffer, p3: Runnable) {
				p3.run()
				val textMessage = new String(p2.buffer().toByteArray)

				val action = textMessage.split(",")
				if(action(0).equals("START")){
					val topic = action(1)
					pool.execute(new Handler(args(0),Integer.parseInt(args(1)),dbConn,topic,runnings,payloads))

				}

			}

			def onConnected() {
				println("connected")
			}

			def onFailure(p1: Throwable) {

			}

			def onDisconnected() {
				println("disconnected")

			}
		})





		val lock = new Object()
		lock.synchronized {
			lock.wait()
		}

	}

	class Handler(host: String, port: Int, dbConn:Connection, topic:String, runnings:mutable.HashMap[String,Boolean], payloads:ListBuffer[Payload]) extends Runnable {
		val LOG = Logger.getLogger(getClass.getName)
		def threadName = Thread.currentThread.getName() +  "-" + topic


		def run() {

			runnings.synchronized {
				if(!runnings.contains(topic)){
					runnings(topic) = true
				}else{
					LOG.info(threadName + " already running.")
					return
				}
			}


			LOG.info(threadName + " is running.")
			val mqttService = new MQTT()
			mqttService.setHost(host, port)
			val connection = mqttService.blockingConnection()
			connection.connect()

			payloads.foreach(p => connection.publish(topic,p.toString.getBytes,QoS.AT_LEAST_ONCE,true))

			connection.disconnect()

			LOG.info(threadName + "finished running.")
			runnings.synchronized {
				runnings.remove(topic)
			}

		}
	}

	class Payload(userId:Int, venueId: Int, rating: Int) {
		override def toString = userId + "," + venueId + "," + rating
	}


}
