package comp90056

import org.fusesource.mqtt.client._
import org.fusesource.hawtbuf.{Buffer, UTF8Buffer}
import java.lang.String


/**
 * User: cq
 * Date: 16/10/13
 * Time: 11:15 PM
 */
object RatingsConsumer {

  def main(args: Array[String]) {
    if(args.length != 3){
      println("usage: RatingsConsumer <host> <port> <topic>")
      System.exit(1)
    }
    val mqtt = new MQTT()
    mqtt.setHost(args(0), Integer.parseInt(args(1)))
    val connection = mqtt.callbackConnection()
    val topic = new Topic(args(2),QoS.AT_LEAST_ONCE)
    val topics = Array[Topic]{topic}
    //    val dbConn = conn

    connection.listener(new Listener {
      def onPublish(p1: UTF8Buffer, p2: Buffer, p3: Runnable) {
        p3.run()
        val textMessage = new String(p2.buffer().toByteArray)

        //split the textMessage to read the fields.
        val values = textMessage.split(",")
        println("userid: " + values(0) + " venueid: " + values(1) + " rating: " + values(2))

      }

      def onConnected() {
        println("connected")
      }

      def onFailure(p1: Throwable) {

      }

      def onDisconnected() {
        println("disconnected")
        //        dbConn.close()
      }
    })

    connection.connect(new Callback[Void] {
      def onFailure(p1: Throwable) {}

      def onSuccess(p1: Void) {
        println("connected")

        connection.subscribe(topics,new Callback[Array[Byte]] {
          def onFailure(p1: Throwable) {
            println("subscription failed!!")
            println(p1.getMessage)
          }

          def onSuccess(p1: Array[Byte]) {
            println("subscribed successfully!!")
          }
        })
      }
    })

    val lock = new Object()
    lock.synchronized {
      lock.wait()
    }

  }
}
